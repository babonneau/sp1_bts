<?php
/**
 * Description Page de consultation de la liste des établissements
 * -> affiche Uun tableau constitué d'une ligne d'entête et d'une ligne par établissement
 * @author prof
 * @version 2018
 */
namespace vue\groupe;

use vue\VueGenerique;
use modele\metier\Groupe;

class VueListeGroupes extends VueGenerique {
    
    /** @var array iliste des établissements à afficher avec leur nombre d'atttributions */
    private $lesGroupes;
    

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
        <br>
        <table width="55%" cellspacing="0" cellpadding="0" class="tabNonQuadrille" >

            <tr class="enTeteTabNonQuad" >
                <td colspan="4" ><strong>Groupes</strong></td>
            </tr>
            <?php
                   // var_dump($this->lesGroupes);
            foreach ($this->lesGroupes as $unGroupe) {
                
                 $unGroupe2 = $unGroupe['groupe'];                
                $nom = $unGroupe2->getNom();
                ?>
                <tr class="ligneTabNonQuad" >
                    <td width="52%" ><?= $nom ?></td>
                
                <td width="16%" align="center" > 
                        <a href="index.php?controleur=groupes&action=modifier&id=<?= $unGroupe2->getId() ?>" >Modifier</a>
                </td>
                <td width="16%" align="center" > 
                            <a href="index.php?controleur=groupes&action=supprimer&id=<?= $unGroupe2->getId() ?>" >
                               Supprimer
                            </a>
                        </td>
                </tr>
                <?php 
            }
            ?>
        </table>
        <br>
         <a href="index.php?controleur=groupes&action=creer" >
            Création d'un groupe</a >
        <?php 
        include $this->getPied();
    }
    function setLesGroupes($lesGroupes) {
        $this->lesGroupes = $lesGroupes;
    }
}
